import datetime

from django.db import models


class Group(models.Model):
    title = models.CharField(max_length=255, null=False)
    start_time = models.DateTimeField(default=datetime.datetime.today)
    course = models.CharField(max_length=64, null=False, default='Python')

    def __str__(self):
        return super().__str__() + f'{self.title} {self.course}'
