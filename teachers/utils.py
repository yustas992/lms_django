from django.http import HttpResponse
from django.utils import html


def render_list(list_of_objects):
    string_rows = []

    for obj in list_of_objects:
        string_rows.append(str(obj))
    response = HttpResponse("\n".join(string_rows))
    response.headers['Content-Type'] = 'text/plain'
    return response


def render_list_html(list_of_object, extra_data="",
                     no_data_message="<No Records>"):
    string_rows = []
    if extra_data:
        string_rows.append(extra_data)

    for obj in list_of_object:
        string_rows.append(html.escape(str(obj)))

    message = "<br>".join(string_rows)

    if not list_of_object:
        message += '<br>' + html.escape(no_data_message)

    response = HttpResponse(message)
    return response


def render_teachers_list_html(list_of_object, extra_data="",
                              no_data_message="<No Records>"):
    string_rows = []
    if extra_data:
        string_rows.append(extra_data)

    for obj in list_of_object:
        string_rows.append(
            html.escape(str(obj)) + ' ' +
            f'<a href="/teachers/update/{obj.id}">Edit</a>'
        )

    message = "<br>".join(string_rows)

    if not list_of_object:
        message += '<br>' + html.escape(no_data_message)

    response = HttpResponse('<body>%s</body>' % message)
    return response
