from django.db import models
from faker import Faker
import random


class Teacher(models.Model):
    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=64, null=False)
    it_experience = models.IntegerField(null=True)
    email = models.EmailField(max_length=200, null=True, blank=True)
    birth_date = models.DateTimeField(null=True, blank=True)
    phone_number = models.CharField(max_length=16, null=True, blank=True)
    inn = models.PositiveIntegerField(unique=True, null=True)
    group = models.ForeignKey(
        to='groups.Group',
        null=True,
        on_delete=models.SET_NULL,
        related_name='teachers'
    )

    @classmethod
    def get_teachers(cls, count):
        fake = Faker()
        for _ in range(count):
            t = Teacher()
            t.first_name = fake.first_name()
            t.last_name = fake.last_name()
            t.email = fake.email()
            t.phone_number = fake.phone_number()
            t.it_experience = random.randint(1, 10)
            t.birth_date = fake.date_time()

            t.save()

    def __str__(self):
        return f'Teacher({self.id}) {self.first_name} ' \
               f'{self.last_name} (IT experience {self.it_experience} y.)' \
               f'{self.email},{self.phone_number},{self.birth_date}'

    @property
    def name(self):
        return f'{self.first_name} {self.last_name}'
