from django.core.management.base import BaseCommand

from teachers.models import Teacher


class Command(BaseCommand):
    help = u'Создание случайного учителя'

    def add_arguments(self, parser):
        parser.add_argument('count', type=int, help=u'Количество создаваемых пользователей')

    def handle(self, *args, **kwargs):
        count = kwargs['count']
        Teacher.get_teachers(count)
