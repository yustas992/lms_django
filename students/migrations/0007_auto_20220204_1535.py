# Generated by Django 3.2.11 on 2022-02-04 13:35

import datetime
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0006_auto_20220201_2231'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='enroll_date',
            field=models.DateTimeField(default=datetime.datetime.today),
        ),
        migrations.AlterField(
            model_name='student',
            name='phone_number',
            field=models.CharField(max_length=64, validators=[
                django.core.validators.RegexValidator('^(\\+\\d\\d?)?\\(\\d{3}\\)(\\d-?){7}$',
                                                      message='Phone number should be in format +1(111)222-33-44')]),
        ),
    ]
